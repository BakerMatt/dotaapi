//
//  ItemService.swift
//  finalproject
//
//  Created by Spencer Baker on 4/9/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit


class ItemService {
    
    /// Get all the items in DotA
    /// - parameter completion: completion block containing optional `[DotaItem]`.
    class func getAllDotaItems(_ completion: @escaping (_ json: [String: AnyObject]?) -> ()) {
        // example: api.steampowered.com/IEconDOTA2_570/GetGameItems/V001/?key=DEC331914A340AC1974E4CCEADE05DB3&language=en_us
        guard let itemsURL = URL(string: "\(dotaBaseAPI)/\(baseHeroAPI)/GetGameItems/V001/?key=\(APIKey)&language=en_us") else { completion(nil); return }
        NetworkService.makeRequest(itemsURL) { (json, response, error) in
            guard let json = json else {
                completion(nil)
                return
            }
            
            completion(json)
        }
    }
    
    /// Get an image for an item.
    /// - parameter completion: completion block containing optional `UIImage` for the item.
    /// - returns: NSURLSessionDataTask for canceling.
    class func imageForItem(itemID: Int, completion: @escaping (_ itemImage: UIImage?) -> ()) -> URLSessionDataTask? {
        guard let item = Item.fetchItemByID(itemID) else {
            // item isn't in Core Data... just return nothing
            completion(nil)
            return nil
        }
        
        let modifiedItemNameForURL = (item.codeName as NSString).substring(from: 5)
        guard let itemURL = URL(string: "http://cdn.dota2.com/apps/dota2/images/items/\(modifiedItemNameForURL)_lg.png") else {
            completion(nil)
            return nil
        }
        
        return NetworkService.imageFromURL(itemURL) { (image, response, error) in
            completion(image)
        }
    }
    
}
