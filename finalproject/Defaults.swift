//
//  Defaults.swift
//  finalproject
//
//  Created by Spencer Baker on 4/23/16.
//  Copyright © 2016 USU. All rights reserved.
//

import Foundation

private let PopulatedAllHeroes = "PopulatedAllHeroes"
private let PopulatedAllItems = "PopulatedAllItems"

/// Class wrapper around NSUserDefaults
class Defaults {
    
    // MARK: Heroes
    
    /// Has the app populated all heroes of app forever first launch?
    class func populatedAllHeroes() -> Bool {
        return UserDefaults.standard.bool(forKey: PopulatedAllHeroes)
    }
    
    /// Set the app that it has populated all heroes.
    class func setPopulatedAllHeroes() {
        UserDefaults.standard.set(true, forKey: PopulatedAllHeroes)
    }
    
    
    // MARK: Items
    
    /// Has the app populated all items of app forever first launch?
    class func populatedAllItems() -> Bool {
        return UserDefaults.standard.bool(forKey: PopulatedAllItems)
    }
    
    /// Set the app that it has populated all items.
    class func setPopulatedAllItems() {
        UserDefaults.standard.set(true, forKey: PopulatedAllItems)
    }
    
}
