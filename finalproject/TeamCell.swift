//
//  TeamCell.swift
//  DotaAPI
//
//  Created by Matt Baker on 4/21/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit

// @TODO: Find somewhere to place last hits and denies, I already pull it from the API and store it in class DotaPlayerStats

class TeamCell: UITableViewCell {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var heroIcon: UIImageView!
    @IBOutlet weak var userIcon: UIImageView!
    
	@IBOutlet weak var heroLevel: UILabel!
	@IBOutlet weak var heroKills: UILabel!
	@IBOutlet weak var heroDeaths: UILabel!
	@IBOutlet weak var heroAssits: UILabel!
    @IBOutlet weak var heroGPM: UILabel!
    @IBOutlet weak var heroXPM: UILabel!
	
	@IBOutlet weak var heroItemOne: UIImageView!
	@IBOutlet weak var heroItemTwo: UIImageView!
	@IBOutlet weak var heroItemThree: UIImageView!
	@IBOutlet weak var heroItemFour: UIImageView!
	@IBOutlet weak var heroItemFive: UIImageView!
	@IBOutlet weak var heroItemSix: UIImageView!
	
	
    func configureCellWithPlayer(_ player: DotaPlayer) {
        // Set username
        self.userName?.text = "\(player.userName)"
        
        // Set user image
        let iconURL = player.userIconURL
        let url = URL(string: iconURL)
        
        let task = URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) -> Void in
            guard let iconData = try? Data(contentsOf: url!) else { return }
            
            DispatchQueue.main.async {
                self.userIcon?.image = UIImage(data: iconData)
            }
        }) 
        task.resume()
    }
	
	func configureCellWithHero(_ hero: Hero) {
		// Set Hero Image
        hero.image { (heroImage) in
            guard let heroImage = heroImage else { return }
            self.heroIcon.image = heroImage
        }
	}
	
	func configureCellWithPlayerStats(_ player: DotaPlayerStats) {
		// Set player stats
		self.heroLevel?.text = String(player.level)
		self.heroKills?.text = String(player.kills)
		self.heroDeaths?.text = String(player.deaths)
		self.heroAssits?.text = String(player.assists)
        self.heroGPM?.text = String(player.gpm)
        self.heroXPM?.text = String(player.xpm)
	}
    
    func configureCellWithPlayerItems(_ items: [Int]) {
		
		
        // Make player items
		var playerItems = [Item]()
		for index in 0..<items.count {
			guard let item = Item.fetchItemByID(items[index])
				else { continue }
			playerItems.append(item)
		}
		
		// Set player items
		var imageArray = [self.heroItemOne, self.heroItemTwo, self.heroItemThree, self.heroItemFour, self.heroItemFive, self.heroItemSix]
		for index in 0..<playerItems.count {
			playerItems[index].image({ (itemImage) -> () in
				imageArray[index]?.image = itemImage
			})
		}
    }
}
