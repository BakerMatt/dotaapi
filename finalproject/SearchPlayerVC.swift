//
//  SearchMatchVC.swift
//  DotaAPI
//
//  Created by Matt Baker on 3/29/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit

class SearchPlayerVC: UIViewController {
    
    @IBOutlet weak var userProfile: UIButton!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userLastMatch: UILabel!
    var userNameID: String = ""
    
    func setUserInformation(_ dotaPlayer: DotaPlayer) {
        //Username
        userName?.text = dotaPlayer.userName
        
        //Last match played
        userLastMatch?.text = dotaPlayer.lastMatch
        
        //User Icon
        let iconURL = dotaPlayer.userIconURL
        let url = URL(string: iconURL)
        
        let task = URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) -> Void in
            guard let iconData = try? Data(contentsOf: url!)
                else {
                    return
            }
            
            DispatchQueue.main.async {
                self.userIcon?.image = UIImage(data: iconData)
                self.userProfile.setImage(UIImage(data: iconData), for: UIControlState())
            }
        }) 
        task.resume()
    }
    @IBAction func animateUserProfile(_ sender: AnyObject) {
        
        let selection = arc4random_uniform(2)
        let userProfileCenterX = self.userProfile.center.x
        let userProfileCenterY = self.userProfile.center.y
        
        print(selection)
        switch selection {
        case 0:
            UIView.animate(withDuration: 0.75, delay: 0.0, options: [.curveEaseIn], animations: {
                self.userProfile.center.x = 600
                self.userProfile.center.y = 600
                }, completion: {
                    (finished: Bool) in
                    
                    self.userProfile.center.x = -300
                    self.userProfile.center.y = -300
                    
                    UIView.animate(withDuration: 0.75, delay: 0.0, options: [.curveEaseOut], animations: {
                        self.userProfile.center.x = userProfileCenterX
                        self.userProfile.center.y = userProfileCenterY
                        }, completion: nil)
            })
        case 1:
            UIView.animate(withDuration: 1.0, delay: 0.0, options: [.curveEaseIn], animations: {
                self.userProfile.center.y = -100.0
                }, completion: {
                    (finished: Bool) in
                    self.userProfile.center.y = 850.0
                    UIView.animate(withDuration: 0.95, delay: 0.0, options: [.curveEaseOut], animations: {
                        self.userProfile.center.y = userProfileCenterY
                        }, completion: nil)
            })
        default: break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "singlePlayerMatchesSegue" {
            let destinationVC = segue.destination as! SinglePlayerMatchVC
            destinationVC.playerID = userNameID
        }
    }
}

extension SearchPlayerVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.text = "76561198025276627"
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //Get player from ID
        
        searchBar.resignFirstResponder()
        
        guard let playerID = searchBar.text
            else {
                return
        }
        userNameID = playerID
        
        DotaAPIManager.getPlayerByID(playerID)
            { (player) in
                guard let player = player
                    else {
                        return
                }
                self.setUserInformation(player)
        }
    }
}
