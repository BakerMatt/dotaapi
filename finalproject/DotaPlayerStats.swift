//
//  DotaPlayerStats.swift
//  DotaAPI
//
//  Created by Matt Baker on 4/23/16.
//  Copyright © 2016 USU. All rights reserved.
//

import Foundation

struct DotaPlayerStats {
	var level : Int
	var kills : Int
	var deaths : Int
	var assists : Int
	var lastHits : Int
	var denies : Int
	var gpm : Int
	var xpm : Int
}