//
//  Hero+CoreDataProperties.swift
//  finalproject
//
//  Created by Spencer Baker on 4/23/16.
//  Copyright © 2016 USU. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Hero {

    @NSManaged var identifier: NSNumber
    @NSManaged var codeName: String
    @NSManaged var name: String
    @NSManaged var imageData: Data?

}
