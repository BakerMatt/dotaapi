//
//  ViewMatchVC.swift
//  DotaAPI
//
//  Created by Matt Baker on 4/20/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit

class ViewMatchVC: UIViewController {
    
    var matchID: Int = 0
    var matchData: DotaMatch?
    var radiantPlayers = [DotaPlayer]()
    var direPlayers = [DotaPlayer]()
	var radiantHeroes = [Hero]()
	var direHeroes = [Hero]()
	var radiantPlayerStats = [DotaPlayerStats]()
	var direPlayerStats = [DotaPlayerStats]()
    var radiantItems = [[Int]]()
    var direItems = [[Int]]()
	var selectedPlayer: Int = 0
	var selectedTeam: Int = 0
	
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        print("VM-ENTERED: \(matchID)")
        
        self.tableView.backgroundColor  = UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1.0)
        
        // Set up the nibs for the custom cells
        tableView.register(UINib(nibName: "MatchResultsCell", bundle: nil), forCellReuseIdentifier: "MatchResultsCell")
        tableView.register(UINib(nibName: "TeamCell", bundle: nil), forCellReuseIdentifier: "TeamCell")
        tableView.register(UINib(nibName: "SkillBuildsCell", bundle: nil), forCellReuseIdentifier: "SkillBuildsCell")
        
        // Retrieve the data for the given matchID
        DotaAPIManager.getDetailedMatchResults(matchID)
            { (match) in
                guard let match = match else { print("VM-FAILED: Match"); return }
                self.matchData = match
                self.tableView.reloadData()
        }
//        DotaAPIManager.getPlayers(matchID, radiantCompletion: { (radiantPlayers) -> () in
//            guard let radiantPlayers = radiantPlayers else { print("VM-FAILED: Radiant Players"); return }
//            self.radiantPlayers = radiantPlayers
//            self.tableView.reloadData()
//            }) { (direPlayers) -> () in
//                guard let direPlayers = direPlayers else { print("VM-FAILED: Dire Players"); return }
//                self.direPlayers = direPlayers
//                self.tableView.reloadData()
//        }
        DotaAPIManager.getRadiantPlayers(matchID)
            { (players) in
                guard let players = players else { print("VM-FAILED: Radiant"); return }
                self.radiantPlayers = players
                self.tableView.reloadData()
        }
        DotaAPIManager.getDirePlayers(matchID)
            { (players) in
                guard let players = players else { print("VM-FAILED: Dire"); return }
                self.direPlayers = players
                self.tableView.reloadData()
        }
		DotaAPIManager.getHeroes(matchID, radiantCompletion: { (radiantHeroes) -> () in
				guard let radiantHeroes = radiantHeroes else { print("VM-FAILED: Radiant Hero Icons"); return }
				self.radiantHeroes = radiantHeroes
				self.tableView.reloadData()
			}) { (direHeroes) -> () in
				guard let direHeroes = direHeroes else { print("VM-FAILED: Dire Hero Icons"); return }
				self.direHeroes = direHeroes
				self.tableView.reloadData()
		}
		DotaAPIManager.getHeroStats(matchID, radiantCompletion: { (radiantPlayerStats) -> () in
				guard let radiantPlayerStats = radiantPlayerStats else { print("VM-FAILED: Radiant Player Stats"); return }
				self.radiantPlayerStats = radiantPlayerStats
				self.tableView.reloadData()
			}) { (direPlayerStats) -> () in
				guard let direPlayerStats = direPlayerStats else { print("VM-FAILED: Dire Player Stats"); return }
				self.direPlayerStats = direPlayerStats
				self.tableView.reloadData()
		}
		DotaAPIManager.getHeroItems(matchID, radiantCompletion: { (radiantItems) -> () in
				guard let radiantItems = radiantItems else { print("VM-FAILED: Radiant Hero Items"); return }
				self.radiantItems = radiantItems
				self.tableView.reloadData()
			}) { (direItems) -> () in
				guard let direItems = direItems else { print("VM-FAILED: Dire Hero Items"); return }
				self.direItems = direItems
				self.tableView.reloadData()
		}
		
		//@TODO Get hero skillbuilds
		
    }
    
    @IBAction func backButton(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "playerMatchesSegue" {
			let destinationVC = segue.destination as! SinglePlayerMatchVC
			if selectedTeam == 1 {
				destinationVC.playerID = radiantPlayers[selectedPlayer].accountID
			} else if selectedTeam == 2 {
				destinationVC.playerID = direPlayers[selectedPlayer].accountID
			}
		}
		
	}
}

extension ViewMatchVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        // Section: Match Details
        case 0:
            return 1
            
        // Section: Radiant Team
        case 1:
            return radiantPlayers.count
            
        // Section: Dire Team
        case 2:
            return direPlayers.count
            
        // Section: Skill Build for both teams
        default:
            return radiantPlayers.count + direPlayers.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        // Section: Match Details
        case 0:
            return 62
            
        // Section: Radiant Team
        case 1:
            return 124
            
        // Section: Dire Team
        case 2:
            return 124
        
        // Section: Skill Build for both teams
        default:
            return 62
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
        header.alpha = 0.75
        
        switch section {
            // Section: Match Details
        case 0:
            header.textLabel!.textColor = UIColor.white
            
            // Section: Radiant Team
        case 1:
            header.textLabel!.textColor = UIColor.green
            
            // Section: Dire Team
        case 2:
            header.textLabel!.textColor = UIColor.red
            
            // Section: Skill Build for both teams
        default:
            header.textLabel!.textColor = UIColor.white
        }
    }
	
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
            // Section: Match Details
        case 0:
            return "Match Results"
            
            // Section: Radiant Team
        case 1:
            return "Radiant Team"
            
            // Section: Dire Team
        case 2:
            return "Dire Team"
            
            // Section: Skill Build for both teams
        default:
            return "Skill Builds"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        // Section: Match Details
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MatchResultsCell", for: indexPath) as! MatchResultsCell
            if matchData == nil { return cell }
            cell.configureCellWithMatch(matchData!)
            return cell
            
        // Section: Radiant Team (Players, Hero Icons, Player Stats, Player Items)
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamCell", for: indexPath) as! TeamCell
            if radiantPlayers.count != 0 { cell.configureCellWithPlayer(radiantPlayers[indexPath.row]) }
			if radiantHeroes.count != 0 { cell.configureCellWithHero(radiantHeroes[indexPath.row]) }
			if radiantPlayerStats.count != 0 { cell.configureCellWithPlayerStats(radiantPlayerStats[indexPath.row]) }
			if radiantItems.count != 0 { cell.configureCellWithPlayerItems(radiantItems[indexPath.row]) }
			
            return cell
            
        // Section: Dire Team (Players, Hero Icons, Player Stats, Player Items)
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamCell", for: indexPath) as! TeamCell
            if direPlayers.count != 0 { cell.configureCellWithPlayer(direPlayers[indexPath.row]) }
			if direHeroes.count != 0 { cell.configureCellWithHero(direHeroes[indexPath.row]) }
			if direPlayerStats.count != 0 { cell.configureCellWithPlayerStats(direPlayerStats[indexPath.row]) }
			if direItems.count != 0 { cell.configureCellWithPlayerItems(direItems[indexPath.row]) }
			
            return cell
            
        // Section: Skill Build for both teams
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SkillBuildsCell", for: indexPath) as! SkillBuildsCell
            if matchData == nil { return cell }
            cell.configureCellWithMatch(matchData!)
            return cell
        }
    }
}

extension ViewMatchVC: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		selectedPlayer = indexPath.row
		selectedTeam = indexPath.section
		if selectedTeam == 1 || selectedTeam == 2 {
			performSegue(withIdentifier: "playerMatchesSegue", sender: self)
		}
	}
}
