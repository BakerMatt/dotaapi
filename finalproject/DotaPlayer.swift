//
//  DotaPlayer.swift
//  DotaAPI
//
//  Created by Matt Baker on 4/5/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit

struct DotaPlayer {
    var userName : String   //Users current alias
	var accountID : String  //Users account ID, used with the API
    var lastMatch : String  //Currently not being used
    var userIconURL : String //Users current icon
}
