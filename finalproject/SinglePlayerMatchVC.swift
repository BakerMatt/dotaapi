//
//  SinglePlayerVC.swift
//  DotaAPI
//
//  Created by Matt Baker on 4/9/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit

class SinglePlayerMatchVC: UIViewController {
    
    var matchData = [DotaMatch]()
    var playerID: String = ""
    var selectedMatch = 0
    @IBOutlet weak var matchTableView: UITableView!
    
    override func viewDidLoad() {
        
        self.matchTableView.backgroundColor  = UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1.0)
        
        super.viewDidLoad()
		        DotaAPIManager.getPlayerMatches(playerID)
            { (matches) in
				guard let matches = matches else { print("SP-FAILED: Match Retrieval"); return }
                self.matchData = matches
                self.matchData.sort() { $0.matchID > $1.matchID }
                self.matchTableView.reloadData()
        }
    }
    
    @IBAction func backButton(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewMatch" {
            let destinationVC = segue.destination as! ViewMatchVC
            destinationVC.matchID = matchData[selectedMatch].matchID
        }
    }
}

extension SinglePlayerMatchVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchmatchcell", for: indexPath) as! SearchMatchCell
        
        cell.configureCellWithMatch(matchData[indexPath.row])
        
        return cell
    }
}

extension SinglePlayerMatchVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMatch = indexPath.row
        performSegue(withIdentifier: "viewMatch", sender: self)
    }
}
