//
//  MatchResultsCell.swift
//  DotaAPI
//
//  Created by Matt Baker on 4/21/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit

class MatchResultsCell: UITableViewCell {
    
    @IBOutlet weak var matchIDLabel: UILabel!
    @IBOutlet weak var winnerLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    func configureCellWithMatch(_ dotaMatch: DotaMatch) {
        // Set matchID label
        self.matchIDLabel?.text = String(dotaMatch.matchID)
        
        // Set winner label
        if dotaMatch.radiantWin {
            self.winnerLabel?.text = "Radiant Victory"
            self.winnerLabel?.textColor = UIColor.green
        } else {
            self.winnerLabel?.text = "Dire Victory"
            self.winnerLabel?.textColor = UIColor.red
        }
        
        // Set duration label
        let minutes: Int = dotaMatch.duration/60
        let seconds: Int = dotaMatch.duration%60
        if seconds < 10 {
            self.durationLabel?.text = "\(minutes):0\(seconds)"
        } else {
            self.durationLabel?.text = "\(minutes):\(seconds)"
        }
    }
    
}
