//
//  SearchMatchVC.swift
//  DotaAPI
//
//  Created by Matt Baker on 3/29/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit

class SearchMatchVC: UIViewController {
    
    var matchData = [DotaMatch]()
    var selectedMatch = 0
    @IBOutlet weak var matchTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.matchTableView.backgroundColor  = UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1.0)
        
        DotaAPIManager.getMatches()
            { (matches) in
                guard let matches = matches else { return }
                self.matchData = matches
                self.matchData.sort() { $0.matchID > $1.matchID }
                self.matchTableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewMatch" {
            let destinationVC = segue.destination as! ViewMatchVC
            destinationVC.matchID = selectedMatch
        }
    }
}

extension SearchMatchVC: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.text = "2215922202"
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        
        guard let match = searchBar.text else { return }
        selectedMatch = Int(match)!
        performSegue(withIdentifier: "viewMatch", sender: self)
    }
}

extension SearchMatchVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchmatchcell", for: indexPath) as! SearchMatchCell
        
        cell.configureCellWithMatch(matchData[indexPath.row])
        
        return cell
    }
}

extension SearchMatchVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMatch = matchData[indexPath.row].matchID
        performSegue(withIdentifier: "viewMatch", sender: self)
    }
}
