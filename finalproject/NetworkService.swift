//
//  NetworkService.swift
//  wordschain
//
//  Created by Spencer Baker on 8/15/15.
//  Copyright (c) 2015 BakerCrew Games. All rights reserved.
//
//  Permission received to use code from: Spencer Baker - Apr 9, 2016

import UIKit


class NetworkService {
	class func makeRequest(_ url: URL, completion: @escaping (_ json: [String: AnyObject]?, _ response: URLResponse?, _ error: NSError?) -> ()) {
		let urlconfig = URLSessionConfiguration.default
		let session = URLSession(configuration: urlconfig)
		
		DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: { () -> Void in
			let dataTask = session.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
				do {
					var jsonData: [String: AnyObject]?
					if let data = data {
						jsonData = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
					}
					DispatchQueue.main.async(execute: {
						completion(jsonData, response, error as NSError?)
					})
				} catch {
					DispatchQueue.main.async(execute: {
						completion(nil, nil, NSError(domain: "Request Error", code: 0, userInfo: [NSLocalizedDescriptionKey : "Failed parsing JSON."]))
					})
				}
			})
			dataTask.resume()
		})
	}
	
	class func makePostRequest(_ url: URL, postData: Data, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ()) {
		let session = URLSession.shared
		
		let request = NSMutableURLRequest(url: url)
		//var request = URLRequest(url: url)
		request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
		request.httpMethod = "POST"
		request.setValue("\(postData.count)", forHTTPHeaderField: "Content-Length")
		request.httpBody = postData
		
		DispatchQueue.global(qos: .userInitiated).async {
			let postRequest = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
				DispatchQueue.main.async {
					completion(data, response, error)
				}
			})
			postRequest.resume()
		}
	}
    
    class func imageFromURL(_ url: URL, completion: @escaping (_ image: UIImage?, _ response: URLResponse?, _ error: NSError?) -> ()) -> URLSessionDataTask {
        let urlconfig = URLSessionConfiguration.default
        let session = URLSession(configuration: urlconfig)
        
        let dataTask = session.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, response, error as NSError?)
                }
                return
            }
            let image = UIImage(data: data)
            DispatchQueue.main.async {
                completion(image, response, nil)
            }
        })
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: { () -> Void in
            dataTask.resume()
        })
        return dataTask
    }
    
}
