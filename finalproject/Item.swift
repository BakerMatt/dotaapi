//
//  Item.swift
//  finalproject
//
//  Created by Spencer Baker on 4/23/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit
import CoreData


class Item: NSManagedObject {
    
    var id: Int {
        return identifier.intValue
    }
    
    /// Image of the item.
    fileprivate var itemImage: UIImage? {
        guard let imageData = imageData else { return nil }
        return UIImage(data: imageData as Data)
    }
    
    /// Get this item's image.
    func image(_ completion: @escaping (_ itemImage: UIImage?) -> ()) {
        if let itemImage = itemImage {
            completion(itemImage)
            return
        }
        
        ItemService.imageForItem(itemID: id) { (itemImage) in
            if let itemImage = itemImage,
                let itemImageData = UIImagePNGRepresentation(itemImage) {
                self.imageData = itemImageData
                mainContext.saveContext()
            }
            completion(itemImage)
        }
    }
    
    /// Get an item by ID. Will first try to find it in the DB. If it doesn't exist, will go to network to try to find it.
    /// **Completion is only called if an item is found.**
    ///
    /// - parameter itemID: The ID of the item you want.
    /// - parameter calledBefore: Bool, call with false or don't fill it in. This is for recursion.
    /// - parameter completion: completion block called if the item was found.
    class func itemByID(_ itemID: Int, calledBefore: Bool = false, completion: @escaping (_ item: Item) -> ()) {
        DispatchQueue.main.async {
            guard let item = fetchItemByID(itemID) else {
                guard !calledBefore else {
                    // called before, don't go into an infinite loop
                    return
                }
                
                // go to network and refresh all the items because this one isn't found
                updateAllItems {
                    itemByID(itemID, calledBefore: true, completion: completion)
                }
                return
            }
            
            completion(item)
        }
    }
    
    /// Update all the items in the DB.
    /// This will go to the network for the refresh.
    class func updateAllItems(_ completion: @escaping () -> ()) {
        ItemService.getAllDotaItems { (json) in
            guard let json = json else {
                completion()
                return
            }
            
            updateItemsFromJSON(json)
            completion()
        }
    }
    
    /// Fetch an item by ID from coredata.
    class func fetchItemByID(_ itemID: Int) -> Item? {
        var item: Item?
        mainContext.performAndWait {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Item")
            request.predicate = NSPredicate(format: "identifier == %d", itemID)
            do {
                let itemResult = try mainContext.fetch(request) as? [Item]
                item = itemResult?.first
            } catch _ {}
        }
        
        return item
    }
    
    fileprivate class func updateItemsFromJSON(_ json: [String: AnyObject]?) -> Bool {
        assert(Thread.isMainThread, "Must be ran on the main thread!")
        
        guard let json = json,
            let result = json["result"] as? [String: AnyObject],
            let itemsJSON = result["items"] as? [[String: AnyObject]] else {
                return false
        }
        
        for itemJSON in itemsJSON {
            guard let name = itemJSON["localized_name"] as? String,
                let itemID = itemJSON["id"] as? Int,
                let codeName = itemJSON["name"] as? String,
                let cost = itemJSON["cost"] as? Int else { continue }
            
            insertItem(itemID: itemID, codeName: codeName, name: name, cost: cost)
        }
        
        return true
    }
    
    fileprivate class func insertItem(itemID: Int, codeName: String, name: String, cost: Int) -> Item {
        assert(Thread.isMainThread, "Must be ran on the main thread!")
        
        var item = fetchItemByID(itemID)
        if item == nil {
            // item isn't in the DB, insert a new one
            item = (NSEntityDescription.insertNewObject(forEntityName: "Item", into: mainContext) as! Item)
        }
        
        guard let dotaItem = item else {
            fatalError("Failed to insert item with ID \(itemID)!")
        }

		dotaItem.identifier = NSNumber(integerLiteral: itemID)
        dotaItem.codeName = codeName
        dotaItem.name = name
		dotaItem.cost = NSNumber(integerLiteral: cost)
		
        mainContext.saveContext()
        
        return dotaItem
    }
    
}
