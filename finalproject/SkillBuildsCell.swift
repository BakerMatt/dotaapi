//
//  SkillBuildsCell.swift
//  DotaAPI
//
//  Created by Matt Baker on 4/21/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit

class SkillBuildsCell: UITableViewCell {
    
    @IBOutlet weak var testLabel: UILabel!
    
    func configureCellWithMatch(_ dotaMatch: DotaMatch) {
        self.testLabel?.text = "Skill Builds: \(String(dotaMatch.matchID))"
    }
}
