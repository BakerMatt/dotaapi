//
//  Hero.swift
//  finalproject
//
//  Created by Spencer Baker on 4/23/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit
import CoreData


class Hero: NSManagedObject {
    
    var id: Int {
        return identifier.intValue
    }
    
    /// Image of the hero.
    fileprivate var heroImage: UIImage? {
        guard let imageData = imageData else { return nil }
        return UIImage(data: imageData as Data)
    }
    
    /// Get this hero's image.
    func image(_ completion: @escaping (_ heroImage: UIImage?) -> ()) {
        if let heroImage = heroImage {
            completion(heroImage)
            return
        }
        
        HeroService.imageForHero(heroID: id) { (heroImage) in
            if let heroImage = heroImage,
                let heroImageData = UIImagePNGRepresentation(heroImage) {
                self.imageData = heroImageData
                mainContext.saveContext()
            }
            completion(heroImage)
        }
    }
    
    /// Go to network and refresh all the heroes.
    class func updateAllHeroes(_ completion: @escaping () -> ()) {
        HeroService.getAllDotaHeroes { (json) in
            guard let json = json else {
                completion()
                return
            }
            updateHeroesFromJSON(json)
            completion()
        }
    }
    
    /// Get a hero by ID. Will first try to find in the DB. If doesn't exist, will go to network to try to find it.
    ///
    /// - parameter heroID: Hero's ID
    /// - parameter calledBefore: Bool, call with false or don't fill it in. This is for recursion.
    /// - parameter completion: completion block called if the hero was found.
    class func heroByID(_ heroID: Int, calledBefore: Bool = false, completion: @escaping (_ hero: Hero) -> ()) {
        DispatchQueue.main.async {
            guard let hero = fetchHeroByID(heroID) else {
                guard !calledBefore else {
                    // called before, don't go into an infinite loop
                    return
                }
                
                // go to network and refresh all the heroes
                updateAllHeroes {
                    heroByID(heroID, calledBefore: true, completion: completion)
                }
                return
            }
            
            completion(hero)
        }
    }
    
    fileprivate class func updateHeroesFromJSON(_ json: [String: AnyObject]?) -> Bool {
        assert(Thread.isMainThread, "Must be ran on the main thread!")
        
        guard let json = json,
            let result = json["result"] as? [String: AnyObject],
            let heroesJSON = result["heroes"] as? [[String: AnyObject]] else {
                return false
        }
        
        for heroJSON in heroesJSON {
            guard let name = heroJSON["localized_name"] as? String,
                let heroID = heroJSON["id"] as? Int,
                let codeName = heroJSON["name"] as? String else { continue }
            insertHero(heroID: heroID, codeName: codeName, name: name)
        }
        
        return true
    }
    
    class func insertHero(heroID: Int, codeName: String, name: String) -> Hero {
        assert(Thread.isMainThread, "Must be ran on the main thread!")
        
        var hero = fetchHeroByID(heroID)
        if hero == nil {
            // hero isn't in the DB, insert a new one
            hero = (NSEntityDescription.insertNewObject(forEntityName: "Hero", into: mainContext) as! Hero)
        }
        
        guard let dotaHero = hero else {
            fatalError("Failed to insert hero with ID \(heroID)!")
        }
        
		dotaHero.identifier = NSNumber(integerLiteral: heroID)
        dotaHero.codeName = codeName
        dotaHero.name = name
        
        mainContext.saveContext()
        
        return dotaHero
    }
    
    class func fetchHeroByID(_ heroID: Int) -> Hero? {
        var hero: Hero?
        mainContext.performAndWait {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Hero")
            request.predicate = NSPredicate(format: "identifier == %d", heroID)
            do {
                let heroResult = try mainContext.fetch(request) as? [Hero]
                hero = heroResult?.first
            } catch _ {}
        }
        
        return hero
    }
    
    
    
}
