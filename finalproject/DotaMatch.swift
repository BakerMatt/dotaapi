//
//  DotaMatch.swift
//  DotaAPI
//
//  Created by Matt Baker on 4/5/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit

struct DotaMatch {
    let matchID: Int  // 2266053440
    let radiantWin: Bool  // true = Radiant win; false = Dire win
    let duration: Int // 2664 (Given in seconds)
}
