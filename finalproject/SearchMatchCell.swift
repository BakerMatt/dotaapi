//
//  SearchPlayerCell.swift
//  DotaAPI
//
//  Created by Matt Baker on 3/29/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit

class SearchMatchCell: UITableViewCell {
    
    @IBOutlet weak var matchID: UILabel!
    @IBOutlet weak var matchInfo: UILabel!
    
    func configureCellWithMatch(_ dotaMatch: DotaMatch) {
        //Match ID
        matchID?.text = "\(dotaMatch.matchID)"
        
        //Match Info
        if dotaMatch.radiantWin {
            matchInfo?.text = "Radiant Victory"
            matchInfo?.textColor = UIColor(red: 0/255, green: 120/255, blue: 5/255, alpha: 1.0)
        } else {
            matchInfo?.text = "Dire Victory"
            matchInfo?.textColor = UIColor.red
        }
    }
}
