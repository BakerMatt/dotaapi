//
//  DotaAPIManager.swift
//  DotaAPI
//
//  Created by Matt Baker on 4/5/16.
//  Copyright © 2016 USU. All rights reserved.
//

import Foundation

let dotaBaseAPI = "https://api.steampowered.com"
let baseMatchAPI = "IDOTA2Match_570"
let baseHeroAPI = "IEconDOTA2_570"
let basePlayerAPI = "ISteamUser/GetPlayerSummaries"
let APIKey = "DEC331914A340AC1974E4CCEADE05DB3"
let basicIcon = "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/fe/fef49e7fa7e1997310d705b2a6158ff8dc1cdfeb_full.jpg"

class DotaAPIManager {
    
    // Get the latest generic matches for all of DotA
    class func getRecentMatches(_ completion: @escaping (_ matchIDs: [Int]?) -> ()) {
        guard let matchesURL = URL(string: "\(dotaBaseAPI)/\(baseMatchAPI)/GetMatchHistory/V001/?key=\(APIKey)") else { completion(nil); return }
        
        NetworkService.makeRequest(matchesURL) { (json, response, error) in
            guard let matchJSON = json,
                let matchResults = matchJSON["result"] as? [String : AnyObject],
                let matches = matchResults["matches"] as? [[String : AnyObject]]
            else {
                completion(nil)
                return
            }
            // parse JSON
            let matchLength = matches.count > 10 ? 10 : matches.count
            var recentMatchIDs = [Int]()
            
            for (index, match) in matches.enumerated() {
                guard index < matchLength,
                    let matchID = match["match_id"] as? Int
                else { break }
                
                recentMatchIDs.append(matchID)
            }
            completion(recentMatchIDs)
        }
    }
    // Get the latest matches for all of DotA
    static func getMatches(_ completion: @escaping (_ matches: [DotaMatch]?) -> Void) {
        getRecentMatches { (matchIDs) in
            guard let matchIDs = matchIDs else { completion(nil); return }
            
            var matchDetails = [DotaMatch]()
            var matchCount = matchIDs.count
            
            for matchID in matchIDs {
                guard let matchDetailsURL = URL(string: "\(dotaBaseAPI)/\(baseMatchAPI)/GetMatchDetails/V001/?key=\(APIKey)&match_id=\(matchID)")
                    else {
                        matchCount -= 1
                        continue
                    }
                
                NetworkService.makeRequest(matchDetailsURL, completion: { (json, response, error) in
                    guard let matchJSON = json,
                        let singleMatchDetail = matchJSON["result"] as? [String: AnyObject],
                        let radiantWin = singleMatchDetail["radiant_win"] as? Bool
                    else {
                        matchCount -= 1
                        completion(nil)
                        return
                    }
                    let match = DotaMatch(matchID: matchID, radiantWin: radiantWin, duration: 0)
                    
                    matchDetails.append(match)
                    
                    if matchDetails.count == matchCount {
                        completion(matchDetails)
                    }
                })
            }
        }
    }
    
    // Get the latest matches for the specific playerID
    class func getRecentPlayerMatches(_ playerID: String, completion: @escaping (_ matchIDs: [Int]?) -> ()) {
        guard let matchesURL = URL(string: "\(dotaBaseAPI)/\(baseMatchAPI)/GetMatchHistory/V001/?key=\(APIKey)&account_id=\(playerID)") else { completion(nil); return }
        NetworkService.makeRequest(matchesURL) { (json, response, error) in
            guard let matchJSON = json,
                let matchResults = matchJSON["result"] as? [String : AnyObject],
                let matches = matchResults["matches"] as? [[String : AnyObject]]
                else {
                    completion(nil)
                    return
            }
            // parse JSON
            let matchLength = matches.count > 20 ? 20 : matches.count
            var recentMatchIDs = [Int]()
            
            for (index, match) in matches.enumerated() {
                guard index < matchLength,
                    let matchID = match["match_id"] as? Int
                    else { break }
                
                recentMatchIDs.append(matchID)
            }
            completion(recentMatchIDs)
        }
    }
    // Get the latest matches for a specific playerID
    static func getPlayerMatches(_ playerID: String, completion: @escaping (_ matches: [DotaMatch]?) -> Void) {
        getRecentPlayerMatches(playerID)
            { (matchIDs) in
                guard let matchIDs = matchIDs
                    else {
                        completion(nil)
                        return
                }
            
            var matchDetails = [DotaMatch]()
            var matchCount = matchIDs.count
            
            for matchID in matchIDs {
                guard let matchDetailsURL = URL(string: "\(dotaBaseAPI)/\(baseMatchAPI)/GetMatchDetails/V001/?key=\(APIKey)&match_id=\(matchID)")
                    else {
                        matchCount -= 1
                        continue
                }
                NetworkService.makeRequest(matchDetailsURL, completion: { (json, response, error) in
                    guard let matchJSON = json,
                        let singleMatchDetail = matchJSON["result"] as? [String: AnyObject],
                        let radiantWin = singleMatchDetail["radiant_win"] as? Bool
                        else {
                            matchCount -= 1
                            completion(matchDetails) // completion was nil
                            return
                    }
                    let match = DotaMatch(matchID: matchID, radiantWin: radiantWin, duration: 0)
                    
                    matchDetails.append(match)
                    
                    if matchDetails.count == matchCount {
                        completion(matchDetails)
                    }
                })
            }
        }
    }
    
    // Get detailed results for a specific matchID
    static func getDetailedMatchResults(_ matchID: Int, completion: @escaping (_ results: DotaMatch?) -> Void) {
        guard let matchDetailsURL = URL(string: "\(dotaBaseAPI)/\(baseMatchAPI)/GetMatchDetails/V001/?key=\(APIKey)&match_id=\(matchID)")
            else {
                return
        }
        
        NetworkService.makeRequest(matchDetailsURL, completion: { (json, response, error) in
            guard let matchJSON = json,
                let singleMatchDetail = matchJSON["result"] as? [String: AnyObject],
                let radiantWin = singleMatchDetail["radiant_win"] as? Bool,
                let duration = singleMatchDetail["duration"] as? Int
                else {
                    completion(nil)
                    return
            }
            let match = DotaMatch(matchID: matchID, radiantWin: radiantWin, duration: duration)
            
            completion(match)
        })
    }
    
    // Get detailed stats about a user given a userID
    static func getPlayerByID(_ userID: String, completion: @escaping (_ player: DotaPlayer?) -> Void) {
        guard let playerURL = URL(string: "\(dotaBaseAPI)/\(basePlayerAPI)/V0002/?key=\(APIKey)&steamids=\(userID)") else { completion(nil); return }
        
        NetworkService.makeRequest(playerURL) { (json, response, error) in
            guard let playerJSON = json,
                let playerResponse = playerJSON["response"] as? [String : AnyObject],
                let players = playerResponse["players"] as? [[String : AnyObject]],
                let player = players.first,
                let userName = player["personaname"] as? String,
                let userIcon = player["avatarfull"] as? String
                else {
                    completion(nil)
                    return
            }
            
            let lastMatch: String = "You need to game more DotA"
            
			let dotaPlayer = DotaPlayer(userName: userName, accountID: userID, lastMatch: lastMatch, userIconURL: userIcon)
            completion(dotaPlayer)
        }
    }
    
    // Get all players from a given matchID
    static func getPlayerArray(_ matchID: Int, completion: @escaping (_ players: [[String : AnyObject]]?) -> Void) {
        guard let matchDetailsURL = URL(string: "\(dotaBaseAPI)/\(baseMatchAPI)/GetMatchDetails/V001/?key=\(APIKey)&match_id=\(matchID)")
            else {
                return
        }
        
        NetworkService.makeRequest(matchDetailsURL) { (json, response, error) in
            guard let matchJSON = json,
                let gameResult = matchJSON["result"] as? [String : AnyObject],
                let players = gameResult["players"] as? [[String : AnyObject]]
                else {
                    completion(nil)
                    return
            }
            completion(players)
        }
    }
    
    // Get all radiant players from a given matchID
    static func getRadiantPlayers(_ matchID: Int, completion: @escaping (_ players: [DotaPlayer]?) -> Void) {
        getPlayerArray(matchID)
            { (players) in
                guard let playerArray = players
                    else {
                        completion(nil)
                        return
                }
                
                let teamSize = playerArray.count/2
				var players = Array(repeating: DotaPlayer(userName: "Anonymous", accountID: "0", lastMatch: "N/A", userIconURL: basicIcon), count: teamSize)
                var playerCount: Int = 0
                
                for index in 0..<teamSize {
                    var player = playerArray[index]
                    playerCount += 1
                    
                    guard let playerSlot = player["player_slot"] as? Int,
                        let accountID = player["account_id"] as? Int
                        else { continue }
                    
                    let convertedSteamID: Int = accountID+76561197960265728
                    
                    getPlayerByID(String(convertedSteamID))
                        { (account) in
                            guard let account = account
                                else { return }
                            
                            if playerSlot < teamSize {
                                players[playerSlot] = account
                                if playerCount >= teamSize {
                                    completion(players)
                                    return
                                }
                            }
                    }
                }
				completion(players) // Added this completion to see if it would solve some bugs of returning nil too often
        }
    }
    
//    // Get all players from a given matchID
//    static func getPlayers(matchID: Int, radiantCompletion: (radiantPlayers: [DotaPlayer]?) -> (), direCompletion: (direPlayers: [DotaPlayer]?) -> ()) {
//        getPlayerArray(matchID)
//            { (players) in
//                guard let playerArray = players
//                    else {
//                        radiantCompletion(radiantPlayers: nil)
//                        direCompletion(direPlayers: nil)
//                        return
//                }
//                
//                var radiantPlayers = [DotaPlayer]()
//                var direPlayers = [DotaPlayer]()
//                var radiantTeamSize: Int = 0
//                var direTeamSize: Int = 0
//                var playerCount: Int = 0
//                
//                // Set team sizes
//                for index in 0..<playerArray.count {
//                    var player = playerArray[index]
//                    
//                    guard let slot = player["player_slot"] as? Int
//                        else { continue }
//                    
//                    if slot < 128 {
//                        radiantTeamSize += 1
//                    } else if slot >= 128 {
//                        direTeamSize += 1
//                    }
//                }
//                
//                // Set players
//                for index in 0..<playerArray.count {
//                    var player = playerArray[index]
//                    playerCount += 1
//                    
//                    guard let slot = player["player_slot"] as? Int,
//                        accountID = player["account_id"] as? Int
//                        else { continue }
//                    
//                    
//                    let convertedSteamID: Int = accountID+76561197960265728
//                    
//                    getPlayerByID(String(convertedSteamID))
//                        { (account) in
//                            guard let account = account
//                                else { return }
//                            
//                            if slot < 128 {
//                                radiantPlayers.append(account)
//                            } else if slot >= 128 {
//                                direPlayers.append(account)
//                            }
//                            
//                            if playerCount == playerArray.count {
//                                radiantCompletion(radiantPlayers: radiantPlayers)
//                                direCompletion(direPlayers: direPlayers)
//                            }
//                    }
//                }
//        }
//    }
    
    // Get all dire players from a given matchID
    static func getDirePlayers(_ matchID: Int, completion: @escaping (_ players: [DotaPlayer]?) -> Void) {
        getPlayerArray(matchID)
            { (players) in
                guard let playerArray = players
                    else {
                        completion(nil)
                        return
                }
                
                let teamSize = playerArray.count/2
				var players = Array(repeating: DotaPlayer(userName: "Anonymous", accountID: "0", lastMatch: "N/A", userIconURL: basicIcon), count: teamSize)
                var playerCount: Int = 0
                
                for index in teamSize..<playerArray.count {
                    var player = playerArray[index]
                    playerCount += 1
                    
                    guard let slot = player["player_slot"] as? Int,
                        let accountID = player["account_id"] as? Int
                        else { continue }
                    
                    let convertedSteamID: Int = accountID+76561197960265728
                    
                    getPlayerByID(String(convertedSteamID))
                        { (account) in
                            guard let account = account
                                else { return }
                            
                            if 128 <= slot && slot < 128+teamSize {
                                let playerSlot = slot-128 // Shift slot to match index
                                players[playerSlot] = account
                                if playerCount >= teamSize {
                                    completion(players)
                                    return
                                }
                            }
                    }
                }
				completion(players) // Added this completion to see if it would solve some bugs of returning nil too often
        }
    }
	
	// Get hero id's from a given matchID
	static func getHeroes(_ matchID: Int, radiantCompletion: @escaping (_ radiantHeroes: [Hero]?) -> (), direCompletion: @escaping (_ direHeroes: [Hero]?) -> ()) {
		getPlayerArray(matchID)
			{ (players) in
				guard let playerArray = players
					else {
						radiantCompletion(nil)
						direCompletion(nil)
						return
				}
				
				var radiantHeroes = [Hero]()
				var direHeroes = [Hero]()
				var radiantHeroIDs = [Int]()
				var direHeroIDs = [Int]()
				
				// Set hero ID's for both teams
				for index in 0..<playerArray.count {
					var player = playerArray[index]
					
					guard let slot = player["player_slot"] as? Int,
						let heroID = player["hero_id"] as? Int
						else { continue }
					
					if slot < 128 {
						radiantHeroIDs.append(heroID)
					} else if slot >= 128 {
						direHeroIDs.append(heroID)
					}
				}
				
				// Set radiant heroes
				for index in 0..<radiantHeroIDs.count {
					guard let hero = Hero.fetchHeroByID(radiantHeroIDs[index]) else { return }
					radiantHeroes.append(hero)
				}
				
				// Set dire heroes
				for index in 0..<direHeroIDs.count {
					guard let hero = Hero.fetchHeroByID(direHeroIDs[index]) else { return }
					direHeroes.append(hero)
				}
				radiantCompletion(radiantHeroes)
				direCompletion(direHeroes)
		}
	}
	
	// Get all hero stats from a given matchID
	static func getHeroStats(_ matchID: Int, radiantCompletion: @escaping (_ radiantPlayerStats: [DotaPlayerStats]?) -> (), direCompletion: @escaping (_ direPlayerStats: [DotaPlayerStats]?) -> ()) {
		getPlayerArray(matchID)
			{ (players) in
				guard let playerArray = players
					else {
						radiantCompletion(nil)
						direCompletion(nil)
						return
				}
				
				var radiantPlayerStats = [DotaPlayerStats]()
				var direPlayerStats = [DotaPlayerStats]()
				
				// Set hero stats for both teams
				for index in 0..<playerArray.count {
					var player = playerArray[index]
					
					guard let slot = player["player_slot"] as? Int,
						let level = player["level"] as? Int,
						let kills = player["kills"] as? Int,
						let deaths = player["deaths"] as? Int,
						let assists = player["assists"] as? Int,
						let lastHits = player["last_hits"] as? Int,
						let denies = player["denies"] as? Int,
						let gpm = player["gold_per_min"] as? Int,
						let exp = player["xp_per_min"] as? Int
						else { continue }
					
					if slot < 128 {
						radiantPlayerStats.append(DotaPlayerStats(level: level, kills: kills, deaths: deaths, assists: assists, lastHits: lastHits, denies: denies, gpm: gpm, xpm: exp))
					} else if slot >= 128 {
						direPlayerStats.append(DotaPlayerStats(level: level, kills: kills, deaths: deaths, assists: assists, lastHits: lastHits, denies: denies, gpm: gpm, xpm: exp))
					}
				}
				radiantCompletion(radiantPlayerStats)
				direCompletion(direPlayerStats)
			}
	}
    
    // Get all hero items from a given matchID
    static func getHeroItems(_ matchID: Int, radiantCompletion: @escaping (_ radiantItems: [[Int]]?) -> (), direCompletion: @escaping (_ direItems: [[Int]]?) -> ()) {
        getPlayerArray(matchID)
            { (players) in
                guard let playerArray = players
                    else {
                        radiantCompletion(nil)
                        direCompletion(nil)
                        return
                }
				
                var radiantItems = [[Int]]()
                var direItems = [[Int]]()
                
                // Set hero items for both teams
                for index in 0..<playerArray.count {
                    var player = playerArray[index]
					var items = [Int]()
					
					guard let slot = player["player_slot"] as? Int
						else { continue }
					
					// Set items for specific player
					for itemIndex in 0..<6 {
						guard let item = player["item_\(itemIndex)"] as? Int
							else { continue }
						
						if item == 0 {
							continue
						} else {
							items.append(item)
						}
					}
					
					if slot < 128 {
						radiantItems.append(items)
					} else if slot >= 128 {
						direItems.append(items)
					}
                }
				radiantCompletion(radiantItems)
				direCompletion(direItems)
        }
    }
}
