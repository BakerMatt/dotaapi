//
//  HeroService.swift
//  finalproject
//
//  Created by Spencer Baker on 4/9/16.
//  Copyright © 2016 USU. All rights reserved.
//

import UIKit

class HeroService {
    
    /// Get all the heroes in DotA
    /// - parameter completion: completion block containing optional `[DotaHero]`.
    class func getAllDotaHeroes(_ completion: @escaping (_ json: [String: AnyObject]?) -> ()) {
        // example: api.steampowered.com/IEconDOTA2_570/GetHeroes/v0001/?key=DEC331914A340AC1974E4CCEADE05DB3&language=en_us
        guard let heroesURL = URL(string: "\(dotaBaseAPI)/\(baseHeroAPI)/GetHeroes/V001/?key=\(APIKey)&language=en") else { completion(nil); return }
        NetworkService.makeRequest(heroesURL) { (json, response, error) in
            guard let json = json else {
                completion(nil)
                return
            }
            
            completion(json)
        }
    }
    
    /// Get an image for a hero. (lg.png for big, sb.png for small)
    /// - parameter completion: completion block containing optional `UIImage` for the item.
    /// - returns: NSURLSessionDataTask for canceling.
    class func imageForHero(heroID: Int, completion: @escaping (_ heroImage: UIImage?) -> ()) -> URLSessionDataTask? {
        guard let hero = Hero.fetchHeroByID(heroID) else {
            // hero isn't in Core Data... just return nothing
            completion(nil)
            return nil
        }
        
        let modifiedHeroNameForURL = (hero.codeName as NSString).substring(from: 14)
        guard let itemURL = URL(string: "http://cdn.dota2.com/apps/dota2/images/heroes/\(modifiedHeroNameForURL)_lg.png") else {
            completion(nil)
            return nil
        }
        
        return NetworkService.imageFromURL(itemURL) { (image, response, error) in
            completion(image)
        }
    }
	
	static func getDotaHeroFromID(_ heroID: Int, completion: (_ dotaHero: Hero?) -> Void) {
		//@TODO GetDotaHeroFromID
		// Get corresponding data from coredata
		
		/** sudo-code:
			completion(dotaHero: coreDataHeroes[heroID])
		 */
	}
}
